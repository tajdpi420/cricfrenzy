package com.cricfrenzy.cricfrenzy.activities;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.cricfrenzy.cricfrenzy.R;
import com.cricfrenzy.cricfrenzy.utils.CustomUI;

public class TestActivity extends AppCompatActivity {
    CustomUI customUI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customUI = new CustomUI(this,
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo),
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo),
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo));
        setContentView(customUI);
        customUI.startFoodAnimation(true);

    }
}
