package com.cricfrenzy.cricfrenzy.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cricfrenzy.cricfrenzy.R;
import com.cricfrenzy.cricfrenzy.models.LiveMatchModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by Sumon on 7/25/2017.
 */

public class LiveMatchAdapter extends PagerAdapter {
    private ArrayList<LiveMatchModel> liveMatchList;
    private LayoutInflater inflater;
    private Context context;


    public LiveMatchAdapter(Context context, ArrayList<LiveMatchModel> liveMatchList) {
        this.context = context;
        this.liveMatchList = liveMatchList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return liveMatchList.size();
    }

    ImageView ivTeamOne;
    ImageView ivTeaTwo;
    TextView tvCell1;
    TextView tvCell2;
    TextView tvCell3;
    TextView tvCell4;
    TextView tvCell5;
    TextView tvCell6;
    TextView tvCell7;
    TextView tvCell8;
    TextView tvCell9;
    TextView tvCell10;

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View liveMatchLayout = inflater.inflate(R.layout.live_match_item_layout, view, false);

        assert liveMatchLayout != null;
        ivTeamOne = (ImageView) liveMatchLayout.findViewById(R.id.ivTeam1);
        ivTeaTwo = (ImageView) liveMatchLayout.findViewById(R.id.ivTeam2);
        tvCell1 = (TextView) liveMatchLayout.findViewById(R.id.tvCell1);
        tvCell2 = (TextView) liveMatchLayout.findViewById(R.id.tvCell2);
        tvCell3 = (TextView) liveMatchLayout.findViewById(R.id.tvCell3);
        tvCell4 = (TextView) liveMatchLayout.findViewById(R.id.tvCell4);
        tvCell5 = (TextView) liveMatchLayout.findViewById(R.id.tvCell5);
        tvCell6 = (TextView) liveMatchLayout.findViewById(R.id.tvCell6);
        tvCell7 = (TextView) liveMatchLayout.findViewById(R.id.tvCell7);
        tvCell8 = (TextView) liveMatchLayout.findViewById(R.id.tvCell8);
        tvCell9 = (TextView) liveMatchLayout.findViewById(R.id.tvCell9);
        tvCell10 = (TextView) liveMatchLayout.findViewById(R.id.tvCell10);
        loadData(position);
        view.addView(liveMatchLayout, 0);
        return liveMatchLayout;
    }

    private void loadData(int position) {
        Picasso.with(context).load(liveMatchList.get(position).getTeamOneLogo()).placeholder(R.drawable.team)// Place holder image from drawable folder
                .error(R.drawable.team).resize(80, 80).centerCrop().into(ivTeamOne);
        Picasso.with(context).load(liveMatchList.get(position).getTeamTwoLogo()).placeholder(R.drawable.team)// Place holder image from drawable folder
                .error(R.drawable.team).resize(80, 80).centerCrop().into(ivTeamOne);
        tvCell1.setText(liveMatchList.get(position).getTextCell1());
        tvCell2.setText(liveMatchList.get(position).getTextCell2());
        tvCell3.setText(liveMatchList.get(position).getTextCell3());
        tvCell4.setText(liveMatchList.get(position).getTextCell4());
        tvCell5.setText(liveMatchList.get(position).getTextCell5());
        tvCell6.setText(liveMatchList.get(position).getTextCell6());
        tvCell7.setText(liveMatchList.get(position).getTextCell7());
        tvCell8.setText(liveMatchList.get(position).getTextCell8());
        tvCell9.setText(liveMatchList.get(position).getTextCell9());
        tvCell10.setText(liveMatchList.get(position).getTextCell10());
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
