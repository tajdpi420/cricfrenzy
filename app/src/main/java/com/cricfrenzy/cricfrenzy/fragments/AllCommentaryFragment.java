package com.cricfrenzy.cricfrenzy.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cricfrenzy.cricfrenzy.R;
import com.cricfrenzy.cricfrenzy.adapters.HomeTabPagerAdapter;

/**
 * Created by Sumon on 7/26/2017.
 */

public class AllCommentaryFragment extends Fragment {
    Activity activity;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_commentary, container, false);
        activity = getActivity();
        return view;
    }
}