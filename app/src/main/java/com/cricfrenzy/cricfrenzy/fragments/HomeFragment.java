package com.cricfrenzy.cricfrenzy.fragments;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cricfrenzy.cricfrenzy.R;
import com.cricfrenzy.cricfrenzy.adapters.HomeTabPagerAdapter;

public class HomeFragment extends Fragment {
    Activity activity;

    private ViewPager mViewPager1,mViewPager2;
    private HomeTabPagerAdapter homeTabPagerAdapter;
    private TabLayout tabLayout1,tabLayout2;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        activity = getActivity();

        mViewPager1 = (ViewPager) view.findViewById(R.id.container);
        setHomeTab(mViewPager1);
        tabLayout1 = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout1.setTabTextColors(Color.WHITE, Color.WHITE);
        tabLayout1.setupWithViewPager(mViewPager1, true);

        mViewPager2 = (ViewPager) view.findViewById(R.id.containerCommentary);
        setCommentaryFragmentToTabs(mViewPager2);
        tabLayout2 = (TabLayout) view.findViewById(R.id.tabsCommentary);
        tabLayout2.setTabTextColors(Color.WHITE, Color.WHITE);
        tabLayout2.setupWithViewPager(mViewPager2, true);

        initTabDivider1();
        initTabDivider2();
        tabLayout1.getTabAt(0).select();
        tabLayout2.getTabAt(0).select();
        return view;
    }

    private void initTabDivider2() {
        for (int i = 0; i < tabLayout2.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout2.getTabAt(i);
            RelativeLayout relativeLayout = (RelativeLayout)
                    LayoutInflater.from(activity).inflate(R.layout.commentary_tab_layout, tabLayout2, false);

            TextView tabTextView = (TextView) relativeLayout.findViewById(R.id.tvCommentary);
            View tenpV = (View) relativeLayout.findViewById(R.id.vTempCommentary);
            if (i == 0) {
                tenpV.setVisibility(View.GONE);
            }
            tabTextView.setText(tab.getText());
            tab.setCustomView(relativeLayout);
            tab.select();
        }
    }

    private void setCommentaryFragmentToTabs(ViewPager mViewPager2) {
        final HomeTabPagerAdapter adapter = new HomeTabPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new AllCommentaryFragment(), getString(R.string.all_tab));
        adapter.addFragment(new KeyMommentsFragment(), getString(R.string.key_moments_tab));
        mViewPager2.setAdapter(adapter);
    }

    private void initTabDivider1() {
        for (int i = 0; i < tabLayout1.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout1.getTabAt(i);
            RelativeLayout relativeLayout = (RelativeLayout)
                    LayoutInflater.from(activity).inflate(R.layout.home_tab_layout, tabLayout1, false);

            TextView tabTextView = (TextView) relativeLayout.findViewById(R.id.tab_title);
            View tenpV = (View) relativeLayout.findViewById(R.id.vTemp);
            if (i == 0) {
                tenpV.setVisibility(View.GONE);
            }
            tabTextView.setText(tab.getText());
            tab.setCustomView(relativeLayout);
            tab.select();
        }
    }


    private void setHomeTab(final ViewPager viewPager) {
        final HomeTabPagerAdapter adapter = new HomeTabPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new LiveFragment(), getString(R.string.live_tab));
        adapter.addFragment(new ResultFragment(), getString(R.string.result_tab));
        adapter.addFragment(new UpcomingFragment(), getString(R.string.upcomming_tab));
        viewPager.setAdapter(adapter);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
    }
}
