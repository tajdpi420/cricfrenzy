package com.cricfrenzy.cricfrenzy.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cricfrenzy.cricfrenzy.R;

/**
 * Created by Sumon on 7/26/2017.
 */

public class KeyMommentsFragment extends Fragment {
    Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_key_momments, container, false);
        activity = getActivity();
        return view;
    }
}
