package com.cricfrenzy.cricfrenzy.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.cricfrenzy.cricfrenzy.R;
import com.cricfrenzy.cricfrenzy.adapters.LiveMatchAdapter;
import com.cricfrenzy.cricfrenzy.models.LiveMatchModel;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sumon on 7/22/2017.
 */
public class LiveFragment extends Fragment {
    Activity activity;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<LiveMatchModel> liveMatchList;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live, container, false);
        activity = getActivity();
        liveMatchList = new ArrayList<>();
        liveMatchList = populateList();

        init(view);
        return view;
    }

    private ArrayList<LiveMatchModel> populateList() {

        ArrayList<LiveMatchModel> list = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            LiveMatchModel imageModel = new LiveMatchModel();
            imageModel.setTextCell1("MATCH 45");
            imageModel.setTextCell2("DD");
            imageModel.setTextCell3("MI");
            imageModel.setTextCell4("66/9");
            imageModel.setTextCell5("213/3");
            imageModel.setTextCell6("RR 4.60");
            imageModel.setTextCell7("RR 10.60");
            imageModel.setTextCell8("13.4/20");
            imageModel.setTextCell9("20/20");
            imageModel.setTextCell10("Mumbai indians won by 146 runs");
            list.add(imageModel);
        }

        return list;
    }

    private void init(View view) {

        mPager = (ViewPager) view.findViewById(R.id.pager);
        mPager.setAdapter(new LiveMatchAdapter(activity, liveMatchList));

        CirclePageIndicator indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = liveMatchList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }
}
