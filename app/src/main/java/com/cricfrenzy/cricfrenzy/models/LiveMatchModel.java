package com.cricfrenzy.cricfrenzy.models;

/**
 * Created by Sumon on 7/25/2017.
 */

public class LiveMatchModel {
    private String textCell1;
    private String textCell2;
    private String textCell3;
    private String textCell4;
    private String textCell5;
    private String textCell6;
    private String textCell7;
    private String textCell8;
    private String textCell9;
    private String textCell10;
    private String teamOneLogo;
    private String teamTwoLogo;

    public LiveMatchModel() {
    }

    public String getTextCell1() {
        return textCell1;
    }

    public void setTextCell1(String textCell1) {
        this.textCell1 = textCell1;
    }

    public String getTextCell2() {
        return textCell2;
    }

    public void setTextCell2(String textCell2) {
        this.textCell2 = textCell2;
    }

    public String getTextCell3() {
        return textCell3;
    }

    public void setTextCell3(String textCell3) {
        this.textCell3 = textCell3;
    }

    public String getTextCell4() {
        return textCell4;
    }

    public void setTextCell4(String textCell4) {
        this.textCell4 = textCell4;
    }

    public String getTextCell5() {
        return textCell5;
    }

    public void setTextCell5(String textCell5) {
        this.textCell5 = textCell5;
    }

    public String getTextCell6() {
        return textCell6;
    }

    public void setTextCell6(String textCell6) {
        this.textCell6 = textCell6;
    }

    public String getTextCell7() {
        return textCell7;
    }

    public void setTextCell7(String textCell7) {
        this.textCell7 = textCell7;
    }

    public String getTextCell8() {
        return textCell8;
    }

    public void setTextCell8(String textCell8) {
        this.textCell8 = textCell8;
    }

    public String getTextCell9() {
        return textCell9;
    }

    public void setTextCell9(String textCell9) {
        this.textCell9 = textCell9;
    }

    public String getTextCell10() {
        return textCell10;
    }

    public void setTextCell10(String textCell10) {
        this.textCell10 = textCell10;
    }

    public String getTeamOneLogo() {
        return teamOneLogo;
    }

    public void setTeamOneLogo(String teamOneLogo) {
        this.teamOneLogo = teamOneLogo;
    }

    public String getTeamTwoLogo() {
        return teamTwoLogo;
    }

    public void setTeamTwoLogo(String teamTwoLogo) {
        this.teamTwoLogo = teamTwoLogo;
    }
}
