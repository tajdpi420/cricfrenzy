package com.cricfrenzy.cricfrenzy.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Rect;
import android.view.View;

/**
 * Created by Sumon on 7/26/2017.
 */

public class CustomUI extends View {
    Path sPath;
    Paint pathPaint;
    PathMeasure pm;
    Bitmap foodBitmap, foodBitmap2, foodBitmap3;
    int SNAP_SIZE = 200;


    public CustomUI(Context context, Bitmap one, Bitmap two, Bitmap three) {
        super(context);
        this.foodBitmap = GifCopier.BITMAP_RESIZER(one, SNAP_SIZE, SNAP_SIZE);
        this.foodBitmap2 = GifCopier.BITMAP_RESIZER(two, SNAP_SIZE, SNAP_SIZE);
        this.foodBitmap3 = GifCopier.BITMAP_RESIZER(three, SNAP_SIZE, SNAP_SIZE);
        sPath = new Path();
        pathPaint = new Paint();
        pathPaint.setColor(Color.YELLOW);
        pathPaint.setStyle(Paint.Style.FILL);
        pathPaint.setAntiAlias(true);
        pathPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        pm = new PathMeasure();

    }

    int iCurStep = 0;// current animation step
    Canvas mCanvas;
    int counter = 0;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mCanvas = canvas;
        if (state) {
            if (counter == 0) {
                FlyFood(canvas, foodBitmap3);
            }
            if (counter == 1) {
                FlyFood(canvas, foodBitmap3);
            }
            if (counter == 2) {
                FlyFood(canvas, foodBitmap3);

            }
        }

    }

    private void FlyFood(Canvas canvas, Bitmap bitmap) {
        sPath.moveTo(getWidth() / 2, getHeight());
        sPath.lineTo(getWidth() / 2, 0);
        pm.setPath(sPath, false);
        float fSegmentLen = pm.getLength() / 300;//we'll get 300 points from path to animate the circle
        float afP[] = {0f, 0f};

        if (iCurStep <= 300) {
            pm.getPosTan(fSegmentLen * iCurStep, afP, null);
            canvas.drawBitmap(bitmap, afP[0] - bitmap.getWidth() / 2, afP[1] - bitmap.getHeight() / 2, pathPaint);
            iCurStep++;
            invalidate();

        } else {
            iCurStep = 0;
            counter++;
            if (counter > 2) {
                ///// all finish here
                state = false;
            } else {
                invalidate();
            }

        }
    }

    boolean state = false;

    public void startFoodAnimation(boolean state) {
        this.state = state;
        invalidate();
    }

    public void drawRectangle(Rect rect) {
        pathPaint.setStyle(Paint.Style.STROKE);
        mCanvas.drawRect(rect, pathPaint);
    }

}